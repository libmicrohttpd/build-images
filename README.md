# Building CI images

This sub-project generates and pushes to gitlab.com
docker registry the docker images to be used for compiling the
master branch of libmicrohttpd.

The reason for pre-generating the images is to speed-up CI runs
and avoid failures due to downloading of individual packages (e.g.,
because some mirrors were down).


# How to generate a new image

Add a new directory with a Dockerfile containing the instructions
for the image.

Then edit .gitlab-ci.yml to add the build instructions, commit and push.


# How to re-generate an existing image

Visit the [CI/CD page](https://gitlab.com/libmicrohttpd/build-images/pipelines) of the
Gitlab project and press the 'Run Pipeline' button.


# Building locally (example for buildenv-debian-stable)
docker login registry.gitlab.com
docker build -t registry.gitlab.com/libmicrohttpd/build-images:buildenv-debian-stable docker-debian-stable
docker push registry.gitlab.com/libmicrohttpd/build-images:buildenv-debian-stable


# Build and test outside Gitlab registry
$ docker build docker-debian-stable
...
Successfully built 54b10afffc6c
$ docker run -it 54b10afffc6c /bin/bash
# git clone https://gitlab.com/libmicrohttpd/libmicrohttpd.git
# cd libmicrohttp
...
